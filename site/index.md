title: Decisive
template: layout.html
siteroot: .
style: |
  .collection-item.bot {
      background: #FFF;
  }
  .collection-item.me {
      background: #DDD;
  }
  #container {
      max-height: 100vh;
  }
  #chatbox {
      max-height: 50vh;
      overflow: auto;
  }

---

<div id="container" class="col s12">
  <h1> Decisive </h1>
  <p> A friendly chatbot for helping people decide from too many options. </p>
  <ul id="chatbox" class="collection">
  </ul>
  <div class="row">
    <form class="input-field" name="act-send">
      <input id="message" required="" class="col s10 validate" type="text">
      <button type="submit" class="col s2 btn">Send</button>
    </form>
  </div>
</div>

{% block head %}
<style>
</style>
{% endblock %}

<script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="js/materialize.min.js"></script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/socket.io/1.3.6/socket.io.min.js"></script>
<script type="text/javascript" src="js/chat.js"></script>
