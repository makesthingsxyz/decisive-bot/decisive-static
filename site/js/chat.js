function scroll_chatbox(){
    chatbox = $("#chatbox");
    chatbox.scrollTop( chatbox.get(0).scrollHeight );
}

$(function () {
    var socket = io();
    $('form').submit(function(){
        msg = $('#message').val();
        socket.send(msg);
        $('#chatbox').append($('<li class="collection-item me">').text(msg));
        $('#message').val('');
        scroll_chatbox();
        return false;
    });
    socket.on('message', function(msg){
        if(msg) {
            $('#chatbox').append($('<li class="collection-item bot">').text(msg));
            scroll_chatbox();
        }
    });
    socket.on('connect', function() {
        socket.send('');
        scroll_chatbox();
    });
});

$(document).ready(scroll_chatbox);
$(window).resize(scroll_chatbox);
