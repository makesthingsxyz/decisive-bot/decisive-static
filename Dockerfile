FROM docker.io/python:alpine
RUN python -V && \
    pip install pipenv
COPY . /app
WORKDIR /app
RUN pipenv install --deploy --system
RUN python3 -m pyj . public
